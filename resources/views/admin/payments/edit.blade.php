@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.payment.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.payments.update", [$payment->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="usermail_id">{{ trans('cruds.payment.fields.usermail') }}</label>
                <select class="form-control select2 {{ $errors->has('usermail') ? 'is-invalid' : '' }}" name="usermail_id" id="usermail_id" required>
                    @foreach($usermails as $id => $usermail)
                        <option value="{{ $id }}" {{ ($payment->usermail ? $payment->usermail->id : old('usermail_id')) == $id ? 'selected' : '' }}>{{ $usermail }}</option>
                    @endforeach
                </select>
                @if($errors->has('usermail'))
                    <div class="invalid-feedback">
                        {{ $errors->first('usermail') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.payment.fields.usermail_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="payment_sum">{{ trans('cruds.payment.fields.payment_sum') }}</label>
                <input class="form-control {{ $errors->has('payment_sum') ? 'is-invalid' : '' }}" type="number" name="payment_sum" id="payment_sum" value="{{ old('payment_sum', $payment->payment_sum) }}" step="1" required>
                @if($errors->has('payment_sum'))
                    <div class="invalid-feedback">
                        {{ $errors->first('payment_sum') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.payment.fields.payment_sum_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="payment_date">{{ trans('cruds.payment.fields.payment_date') }}</label>
                <input class="form-control date {{ $errors->has('payment_date') ? 'is-invalid' : '' }}" type="text" name="payment_date" id="payment_date" value="{{ old('payment_date', $payment->payment_date) }}" required>
                @if($errors->has('payment_date'))
                    <div class="invalid-feedback">
                        {{ $errors->first('payment_date') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.payment.fields.payment_date_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="available_to">{{ trans('cruds.payment.fields.available_to') }}</label>
                <input class="form-control date {{ $errors->has('available_to') ? 'is-invalid' : '' }}" type="text" name="available_to" id="available_to" value="{{ old('available_to', $payment->available_to) }}" required>
                @if($errors->has('available_to'))
                    <div class="invalid-feedback">
                        {{ $errors->first('available_to') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.payment.fields.available_to_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection