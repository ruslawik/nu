@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.group.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.groups.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.group.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.group.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="payment_price">{{ trans('cruds.group.fields.payment_price') }}</label>
                <input class="form-control {{ $errors->has('payment_price') ? 'is-invalid' : '' }}" type="number" name="payment_price" id="payment_price" value="{{ old('payment_price', '0') }}" step="1" required>
                @if($errors->has('payment_price'))
                    <div class="invalid-feedback">
                        {{ $errors->first('payment_price') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.group.fields.payment_price_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="payment_period">{{ trans('cruds.group.fields.payment_period') }}</label>
                <input class="form-control {{ $errors->has('payment_period') ? 'is-invalid' : '' }}" type="number" name="payment_period" id="payment_period" value="{{ old('payment_period', '30') }}" step="1" required>
                @if($errors->has('payment_period'))
                    <div class="invalid-feedback">
                        {{ $errors->first('payment_period') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.group.fields.payment_period_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="description">{{ trans('cruds.group.fields.description') }}</label>
                <input class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" type="text" name="description" id="description" value="{{ old('description', '') }}">
                @if($errors->has('description'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.group.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection