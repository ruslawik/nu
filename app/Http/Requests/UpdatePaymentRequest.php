<?php

namespace App\Http\Requests;

use App\Payment;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdatePaymentRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('payment_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'usermail_id'  => [
                'required',
                'integer'],
            'payment_sum'  => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647'],
            'payment_date' => [
                'required',
                'date_format:' . config('panel.date_format')],
            'available_to' => [
                'required',
                'date_format:' . config('panel.date_format')],
        ];

    }
}
