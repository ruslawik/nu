<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$oqId7bOPsgMx1DrjY8E0eepgB5AhNHfAKo2rLJdWFw1yOLHH6MyTy',
                'remember_token' => null,
                'surname'        => '',
                'last_name'      => '',
                'phone'          => '',
            ],
        ];

        User::insert($users);

    }
}
