<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('payment_price');
            $table->integer('payment_period');
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
