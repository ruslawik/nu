<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::post('users/media', 'UsersApiController@storeMedia')->name('users.storeMedia');
    Route::apiResource('users', 'UsersApiController');
    Route::post('users/login', 'UsersApiController@login');
    Route::get('users/{user_id}/availability', 'UsersApiController@availability');
    Route::get('users/get/withkeys', 'UsersApiController@withkeys');
    Route::get('users/{user_id}/give_key', 'UsersApiController@give_key');
    Route::get('users/{user_id}/take_key', 'UsersApiController@take_key');

    // Payments
    Route::apiResource('payments', 'PaymentsApiController');

    // Groups
    Route::apiResource('groups', 'GroupsApiController');

    // Attendances
    Route::apiResource('attendances', 'AttendanceApiController');

});

/*

Client ID: 1
Client secret: IlGs5ieBRnKQESkiLQSOmO7k57ASdlYf1Cd5YcKJ
Password grant client created successfully.
Client ID: 2
Client secret: ZRraiTdrM6ixRE3cvBrFSDwHMBv7xR8rPLvE3CxN

*/
